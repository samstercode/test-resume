# README

## Overview

* Code resides in `GitLab`
* Use `asciidoctor` CLI to produce HTML version
* Use `asciidoctor-pdf` CLI to produce PDF version
* Uses `Netlify` to build and doploy
* When you push code to `GitLab`, `Netlify` will publish to domain 

## Prerequisites

* [Asciidoctor](https://docs.asciidoctor.org/asciidoctor/latest/install/)
* [Asciidoctor PDF](https://docs.asciidoctor.org/pdf-converter/latest/install/#install-asciidoctor-pdf)
* Setup account on [Netlify](https://www.netlify.com/) and make sure you setup a `site` to basically monitor your `GitLab` repo.

## Steps to Build/Deploy

1. Checkout your code from `GitLab` 
2. Modify `resume.adoc` file and optionally any related style files 
3. Generate HTML version  

        asciidoctor resume.adoc  
   Rename `resume.html` to `index.html`
4. Generate PDF version  

        asciidoctor-pdf --theme default-for-print -a pdf-themesdir=./pdf-resources resume.adoc
   Rename `resume.pdf` to `scottalthaus-resume.pdf`
5. Commit to repo and push to `main` branch
(Netlify should pick up change)